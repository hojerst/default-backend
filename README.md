# Default Backend

This is a simple web server which hosts a 404 error page. It is intended as a default backend in Kubernetes, but might
have other uses.

The following screenshot shows the error page in action (light and dark mode supported).

![Screenshot](screenshot.png)

## Usage

### Docker

```bash
docker run --rm -it -p 8080:80 hojerst/default-backend
```

### Kubernetes

The following snippet will provision a deployment to the current namespace. It will
also provision a Ingress which basically acts a default backend. Note: Ingresses with
matching rules will have higher priority. You can also use the `defaultBackend` feature
of your preferred `IngressController` (e.g. [nginx](https://kubernetes.github.io/ingress-nginx/user-guide/default-backend/)).

```bash
kubectl apply -f https://gitlab.com/hojerst/default-backend/-/raw/master/k8s-manifests.yml
```
