package main

import (
	_ "embed"
	"fmt"
	"net/http"
	"os"
)

var version = "0.0.1-dev"

const PORT = 80

//go:embed public/errors/404.html
var errorPage string

func main() {
	if len(os.Args) >= 2 && os.Args[1] == "--version" {
		fmt.Println("Version", version)
		os.Exit(0)
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-type", "text/html")
		w.WriteHeader(http.StatusNotFound)
		_, _ = fmt.Fprintln(w, errorPage)
	})

	fmt.Println("Listening on port", PORT)
	err := http.ListenAndServe(":80", nil)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
