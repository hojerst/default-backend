FROM golang:1.24.1@sha256:c5adecdb7b3f8c5ca3c88648a861882849cc8b02fed68ece31e25de88ad13418 as build

ARG VERSION=0.0.1-dev

WORKDIR /work
COPY . .

ENV CGO_ENABLED=0

RUN go build -ldflags="-s -X main.version=${VERSION}" -o default-backend .

FROM gcr.io/distroless/base-debian11@sha256:ac69aa622ea5dcbca0803ca877d47d069f51bd4282d5c96977e0390d7d256455

# see: https://github.com/opencontainers/image-spec/blob/v1.0.1/annotations.md
LABEL org.opencontainers.image.title="Default Backend"
LABEL org.opencontainers.image.description="a simple web server which hosts a 404 error page"
LABEL org.opencontainers.image.licenses="MIT"

COPY --from=build /work/default-backend /
EXPOSE 80
ENTRYPOINT [ "/default-backend" ]
